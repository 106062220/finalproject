var Load = {
	preload(){
		var loadingLabel = this.game.add.text(this.game.width/2, 150, 'loading...', {
			font: '30px Arial',
			fill: '#ffffff'
		});
		loadingLabel.anchor.setTo(0.5, 0.5);

		this.game.stage.backgroundColor = '#ffb915';
		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.game.renderer.renderSession.roundPixels = true;

		this.scale.pageAlignHorizontally = true;
		this.scale.pageAlignVertically = true;
		// this.game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
	},

	create(){
		this.game.state.start('menu');
	}
}