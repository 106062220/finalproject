var SinglePlayer = {
	preload(){
		this.game.load.image('player', 'assets/player.png');
		this.game.load.image('arrow', 'assets/arrow.png');
		this.game.load.image('bullet', 'assets/bullet.png');
	},

	create(){

		this.cursor = this.game.input.keyboard.createCursorKeys();
		
		
		this.arrow = this.game.add.sprite(this.game.width/2, this.game.height, 'arrow');
		this.game.physics.arcade.enable(this.arrow);
		this.arrow.anchor.setTo(0,0.5);
		this.arrow.visible = false;


		this.player = this.game.add.sprite(this.game.width/2, this.game.height, 'player');
		this.game.physics.arcade.enable(this.player);
		this.player.body.collideWorldBounds = true;
		this.player.body.gravity.y = 1000;
		this.player.anchor.setTo(0.5,0.5);
		this.player.jumping = false;
		this.player.power = 1;
		this.player.power_ = -1;

		this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(10, 'bullet');

	},

	update(){
		if (this.game.input.mousePointer.isDown)
		{
			this.arrow.visible = true;
			this.arrow.body.x = this.player.body.x+15;
			this.arrow.body.y = this.player.body.y+8;
			this.arrow.rotation = this.game.physics.arcade.angleBetween( this.player,this.game.input);
			
			this.player.power+=0.015*this.player.power_;
			if(this.player.power<=0.1)this.player.power_ = 1;
			else if(this.player.power>=1)this.player.power_ = -1;
			
			this.arrow.scale.setTo(this.player.power,1);

		}		
		else {
			if(this.arrow.visible){
				this.arrow.visible = false;
				this.addbullet();
			}
			this.player.power = 1;
		}
		
			
		




		if(this.player.y>=this.game.height-20)this.land();
		this.movePlayer();
	},

	addbullet(){
		var bullet = this.bullets.getFirstDead();
        if (!bullet) { return;}
        bullet.anchor.setTo(0.5, 0.5);
		bullet.reset(this.player.x, this.player.y);
		bullet.body.gravity.y = 1000; 
		bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;
		this.game.physics.arcade.moveToPointer(bullet, 1000*this.player.power);
	},


	land(){
		this.player.jumping = false;
	},
	
	movePlayer() {
		if (this.cursor.up.isDown&&!this.player.jumping) { 
            if(this.cursor.left.isDown) this.player.animations.play('left');   
            else if(this.cursor.right.isDown)this.player.animations.play('right');
            else if(!this.cursor.left.isDown&&!this.cursor.right.isDown) this.player.animations.play('straight');
			this.player.body.velocity.y = -500;
			
			this.player.jumping = true;
            
        }  
       
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.animations.play('left');
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.animations.play('right');
        }    

        
       
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            

            this.player.animations.play('straight');
            
        } 
    }
}