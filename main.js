window.onload = ()=>{
	var game = new Phaser.Game(1200, 700, Phaser.AUTO, 'game-div');
	
	game.state.add('maps', Maps);
	game.state.add('load', Load);
	game.state.add('menu', Menu);
	game.state.add('twoPlayer', TwoPlayer);
	game.state.add('singlePlayer', SinglePlayer);
	game.state.add('setting', Setting);
	game.state.start('load');
}