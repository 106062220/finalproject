var Menu = {
	preload(){
		this.game.load.image('button', './assets/button_1.jpg', 164, 72);
	},

	create(){
		var button1 = this.game.add.button(980, 85, 'button', ()=>{
			this.game.state.start('singlePlayer');
		}, this);
		button1.anchor.setTo(0.5, 0.5);
		var txt1 = this.game.add.text(980, 85, '單人', {
			font: '24px Arial',
			fill: '#ffffff'
		});
		txt1.anchor.setTo(0.5, 0.5);
		button1.alpha = 0.8;

		var button2 = this.game.add.button(980, 185, 'button', ()=>{
			this.game.state.start('twoPlayer');
		}, this);
		button2.anchor.setTo(0.5, 0.5);
		var txt2 = this.game.add.text(980, 185, '雙人', {
			font: '24px Arial',
			fill: '#ffffff'
		});
		txt2.anchor.setTo(0.5, 0.5);
		button2.alpha = 0.8;

		var button3 = this.game.add.button(980, 285, 'button', ()=>{
			this.game.state.start('setting');
		}, this);
		button3.anchor.setTo(0.5, 0.5);
		var txt3 = this.game.add.text(980, 285, '設定', {
			font: '24px Arial',
			fill: '#ffffff'
		});
		txt3.anchor.setTo(0.5, 0.5);
		button3.alpha = 0.8;

		button1.onInputOver.add(()=>{
			button1.alpha = 1;
		}, this);
		button1.onInputOut.add(()=>{
			button1.alpha = 0.8;
		}, this);

		button2.onInputOver.add(()=>{
			button2.alpha = 1;
		}, this);
		button2.onInputOut.add(()=>{
			button2.alpha = 0.8;
		}, this);

		button3.onInputOver.add(()=>{
			button3.alpha = 1;
		}, this);
		button3.onInputOut.add(()=>{
			button3.alpha = 0.8;
		}, this);


		this.game.add.text(200, 100, '發生在部落之間的衝突', {
			font: '40px Arial',
			fill: '#ffffff'
		});
		
	},

	update(){

	}
}